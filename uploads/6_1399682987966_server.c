// Author: Kevin Smith
// Assignment: Program 1 (part 1)
// Description: Message server that stores a single message and receives
//		new ones through a message queue
// Compilation: gcc server.c -lrt -o server

#include "common.h"
#include <signal.h>

// Function prototypes
void cleanup();

int main (int argc, char *argv[]) {
	// Ensure the program was invoked correctly
	if (argc != 2) {
		fprintf(stderr, "Usage: %s \"Message\"\n", argv[0]);
	}

	// Allocate resources
	message  = malloc(MAX_LENGTH); // For storing the current message
	received = malloc(MAX_LENGTH); // For receiving a client's message
	
	// Copy the argument string to the message buffer
	strcpy(message, argv[1]);
	printf("Initial message: %s\n", message);

	// Set up handler for Ctrl-C
	signal(SIGINT, cleanup);

	// Settings for the queues
	struct mq_attr attr;
	attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = MAX_LENGTH;
    attr.mq_curmsgs = 0;

	// Open both queues
	recv_q = mq_open(SERVER_QUEUE_NAME, O_RDONLY | O_CREAT, 0666, &attr);
	send_q = mq_open(CLIENT_QUEUE_NAME, O_WRONLY | O_CREAT, 0666, &attr);

	// If the queues both opened, loop to receive client messages
	if (recv_q != (mqd_t)-1 && send_q != (mqd_t)-1) {
		while (1) {
			ssize_t msgsize = mq_receive(recv_q, received, MAX_LENGTH, NULL);
			if (msgsize != -1) {
				// Send the client the previous message
				mq_send(send_q, message, strlen(message), 0);

				// Clear the previous message and store the new one
				memset(message, 0, MAX_LENGTH);
				strncpy(message, received, msgsize);

				// Count the client as served
				num_served++;
			} else {
				perror("Error receiving message");
			}
		}
	} else {
		perror("Failed to open message queues");
	}

	// Never reached
	return 0;
}

// Prints out the current message, usage details, closes the message queues, and
// frees the memory used for messages.  Invoked on SIGINT, or Ctrl-C.
void cleanup() {
	int status; // For return values from closing queues

	// Print results
	printf("Current Message: %s\n", message);
	printf("Clients Served: %d\n", num_served);

	// Close both queues
	status = mq_close(recv_q);
	if (status < 0) perror("Failed to close server queue");
	status = mq_close(send_q);
	if (status < 0) perror("Failed to close client queue");

	// Clean up
	free(received);
	free(message);

	// Terminate program
	exit(0);
}