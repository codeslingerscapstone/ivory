// Author: Kevin Smith
// Assignment: Program 1 (part 1)
// Description: Message client that sends a new message to the message
// 		server and displays the previous server message
// Compilation: gcc client.c -lrt -o client

#include "common.h"

int main (int argc, char *argv[]) {
	// Ensure the program was invoked correctly
	if (argc != 2) {
		fprintf(stderr, "Usage: %s \"Message\"\n", argv[0]);
	}

	// Allocate resources
	message = malloc(MAX_LENGTH); // For storing the message
	int status;                   // For the return value of mq_close

	// Copy the argument string to the message buffer
	strcpy(message, argv[1]);

	// Open both queues
	recv_q = mq_open(CLIENT_QUEUE_NAME, O_RDONLY | O_CREAT);
	send_q = mq_open(SERVER_QUEUE_NAME, O_WRONLY          );

	// Send the message
	status = mq_send(send_q, message, strlen(message), 0);
	if (status < 0) perror("Failed to send message");

	// Receive the previous message from the server
	memset(message, 0, MAX_LENGTH);
	ssize_t msgsize = mq_receive(recv_q, message, MAX_LENGTH, NULL);
	if (msgsize != -1) {
		printf("Previous Message: %s\n", message);
	} else {
		perror("Failed to receive response");
	}

	// Close both queues
	status = mq_close(recv_q);
	if (status < 0) perror("Failed to close client queue");
	status = mq_close(send_q);
	if (status < 0) perror("Failed to close server queue");

	// Clean up
	free(message);

	return 0;
}