// Author: Kevin Smith
// Assignment: Program 1 (part 2)
// Description: Calculates the product of some function over a range
// 		by splitting up the work among several processes.
// Compilation: gcc prog1.c f.c -o prog1

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

// Define a bool type for convenience
#define bool int
#define true 1
#define false 0

// Function prototypes
void calculateSubset(int begin, int end, int *pipe);
void readFully(int desc, void *buf, int size);
double f(int x);

int main(int argc, char *argv[]) {
	// Ensure the program was invoked correctly
	if (argc != 3) {
		fprintf(stderr, "Usage: %s <N> <P>\n", argv[0]);
		exit(1);
	}

	// Parse values from command line
	int N = atoi(argv[1]);
	int P = atoi(argv[2]);

	// Make sure arguments are valid
	if (N < 0 || P < 1 || P > (N + 1)) {
		fprintf(stderr, "Constraints: N >= 0 and 1 <= P <= N+1\n");
		exit(1);
	}

	int i; // For iterating in loops

	// Allocate resources
	pid_t *pids    = (pid_t *) malloc(P * sizeof(pid_t));   // Holds child pids
	int   *pipes   = (int *)   malloc(2 * P * sizeof(int)); // Holds child pipes

	// For each child process
	for (i = 0; i < P; i++) {
		// Initialize the child's pipe
		pipe(&pipes[2*i]);

		// Fork the process
		pids[i] = fork();
		if (pids[i] < 0) {
			perror("Pipe creation unsuccessful");
		}

		// If I am the child process
		if (pids[i] == 0) {
			// Find range of work for the child
			int begin = (N * i) / P;
			int end = ((N * (i + 1)) / P) - 1;

			// If this is the last process, add the last number to it
			if (i == P-1) {
				end = N;
			}

			// Perform work and pipe it back to the parent
			calculateSubset(begin, end, &pipes[2*i]);
			break; // Don't let the child continue forking
		}
	}

	// Figure out if I am the parent (if all pids are non-zero)
	bool parent = true;
	for (i = 0; i < P; i++) {
		if (pids[i] == 0) {
			parent = false;
			break;
		}
	}

	// If I am the parent
	if (parent) {
		double *results = (double *)malloc(P * sizeof(double));
		double answer = 1;

		// For each child
		for (i = 0; i < P; i++) {
			close(pipes[2*i+1]); // Close the writing end of the pipe

			// Read a double from the child's pipe
			readFully(pipes[2*i], &results[i], sizeof(double));

			close(pipes[2*i]);   // Close the reading end of the pipe

			// Multiply the double by the total answer
			answer *= results[i];
		}

		// Print the answer
		printf("%f\n", answer);

		// free the results array in the parent
		free(results);
	}

	// Free the other arrays in both the parent and the children
	free(pids);
	free(pipes);

	return 0;
}

// Executed only by a child process, calculates the product of f(x) over
// a range and writes the result to a pipe for the parent process to read.
void calculateSubset(int begin, int end, int *pipe) {
	// Close the reading end of the pipe
	close(pipe[0]);

	// Calculate f(x) for each x in the range
	double product = 1.0;
	double result;
	int i;
	for (i = begin; i <= end; i++) {
		result = f(i);

		// If the result is 0, return immediately
		if (result == 0) {
			write(pipe[1], &result, sizeof(double));
			return;
		}

		// Otherwise, multiply by the product
		product = product * result;
	}

	// Write the answer to the pipe and close the pipe
	write (pipe[1], &product, sizeof(double));
	close(pipe[1]);
}

/*
 * Author: Jeff Donahoo, grand master
 * Reads size bytes from desc into buf
 */
void readFully(int desc, void *buf, int size) {
  int count = 0; // Count of bytes read so far
  int rcvd;      // Byte read in last read()
  while (count < size) {
    rcvd = read(desc, ((char *) buf) + count, size - count);
    if (rcvd < 0) {  // If error
      perror("read() failed");
      exit(1);
    } else if (rcvd == 0) {  // If premature end of stream
      fputs("Unexpected EOS", stderr);
      exit(1);
    }
    count += rcvd;
  }
}