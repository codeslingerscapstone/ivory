#include <iostream>

using namespace std;

int main() {
    int numTimes;
    cin >> numTimes;
    for(int x = 0; x < numTimes; x++) {
        string input;
        cin >> input;
        cout << input << endl;
    }
    return 0;
}
