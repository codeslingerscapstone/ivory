/*
 * Austin Bratcher
 * Cracker Barrel Puzzle
 * Description: Write a program to solve the triangle puzzle 
 *              from crakcer barrel. The program should print
 *              out each move required to solve the puzzle.
 * Due     : 01.20.2012
 * Created : 01.14.2012
 * Modified: 01.15.2012
 */

#include <iostream>
#include <vector>
#include <utility>


using namespace std;
/********************TEE CLASS*******************/
class Tee{
private:
    pair <int,int> location;
    int nextDirection;
    enum iteration {UPRIGHT, 
            RIGHT, LOWRIGHT,
            LOWLEFT, LEFT,
            UPLEFT, DONE};
public:
    /*
     * Precondition: None
     * Postcondition: A custom Tee object is created
     * Return Type: None
     * Description: The function uses to integers to
     *              initialize the location of the 
     *              Tee object. 
     */
    Tee(int,int);
    
    /*
     * Precondition: None
     * Postcondition: None
     * Return Type: Pair<int,int> - represents the locatin of
     *              the Tee object.
     * Description: The function returns the location of the
     *              Tee object. 
     */
    pair <int,int> getLocation();
    
    /*
     * Precondition: None
     * Postcondition: None
     * Return Type: bool - true/false depending on status of
     *              nextDirection. 
     * Description: The function determines whether or not the
     *              Tee object has finished iterating through
     *              its neighbored based upon the status of
     *              nextDirection. 
     */
    bool isDone();
    
    /*
     * Precondition: None
     * Postcondition: nextDirection is updated to show
     *                the next direction the Tee object
     *                should go
     * Return Type: Paid<int,int> - represents the location
     *              of the next place to visit
     * Description: The function uses nextDirection to 
     *              determine the next location the 
     *              Tee object should visit
     */
    pair <int,int> nextMove();
};

/*******************BOARD CLASS******************/
class Board{
private:
    vector<vector<int> > myBoard;
    int numPegs;
    enum item {INVALID,HOLE,PEG};
public:
    /*
     * Precondition: None
     * Postcondition: numPegs and myBoard are
     *                initialized to default values
     * Return Type: None
     * Description: This function serves as a default 
     *              constructor for a Board object
     */
    Board();
    
    /*
     * Precondition: None
     * Postcondition: None
     * Return Type: vect<vector<int> > - represents
     *              myBoard of the Board object
     * Description: The function returns the value held
     *              by myBoard.
     */
    vector<vector<int> > getBoard();
    
    /*
     * Precondition: None
     * Postcondition: None
     * Return Type: int - The number of pegs held in
     *              numPegs
     * Description: The function returns the number of
     *              pegs held by the current Board object.
     */
    int getNumPegs();
    
    /*
     * Precondition: None
     * Postcondition: None
     * Return Type: bool - true/false depending on 
     *              given pair (or location)
     * Description: The function determines whether the
     *              given location holds a peg.
     */
    bool isPeg(pair <int,int>);
    
    /*
     * Precondition: None
     * Postcondition: None
     * Return Type: Bool - depending on presence of
     *              given location in myBoard
     * Description: The function determines whether the
     *              given pair (or location) is contained
     *              within the boundaries of myBoard. 
     */
    bool isValidCoor(pair<int,int>);
    
    /*
     * Precondition: None
     * Postcondition: None
     * Return Type: bool - true/false depending on the
     *              given input
     * Description: The function determines if the move
     *              from the first pair (or location)
     *              to the second is a valid move in the
     *              current Board object
     */
    bool isValidMove(pair <int,int>, pair <int,int>);
    
    /*
     * Precondition: The given pairs represent a valid move
     * Postcondition: The current Board object is modified to
     *                reflect the given move
     * Return Type: None
     * Description: The function moves the peg at the first
     *              pair (or location) to the second and
     *              modifies myBoard to reflect the move.
     */
    void makeMove(pair <int,int>, pair <int,int>);
    
    /*
     * Precondition: None
     * Postcondition: None
     * Return Type: ostream& - creates a chainging effect
     * Description: The function overrides the >> operator
     *              so that a Board object can be printed.
     */
    friend ostream& operator<<(ostream& os, Board b);
};

/**************GAME CLASS***********************/
class Game{
private:
    bool solutionFound;
public:
    /*
     * Precondition: None
     * Postcondition: solutionFound is set to
     *                a default value of false
     * Return Type: None
     * Description: The function is a default 
     *              constructor for a game object
     */
    Game();
    
    /*
     * Precondition: None
     * Postcondition: solutionFound and solution
     *                are both modified to reflect
     *                the results of the function
     * Return Type: None
     * Description: This is a recursive function used
     *              to solve a board by iterating 
     *              through the locations on the board
     *              and calling appropriate modification
     *              methods to reflect the moves of each
     *              recursive call
     */
    void solve(Board);
};


/*
 * Abstract: Creates a default Game and Board object
 *            Uses Game object to solve problem presented
 *            in Board object
 * Input: None
 * Process: A Board object is created and solved using
 *          a Game object
 * Output: Prints the solution to the problem presented
 *         in the Board object
 * Assumptions: None
 */
/*****************MAIN FUNCTION*****************/
int main(){
    Board newBoard;
    Game newGame;
    newGame.solve(newBoard);
    return 0;
}

/**************Tee Functions******************/
Tee::Tee(int y,int x){
    location.first = y;
    location.second = x;
    nextDirection = UPRIGHT;
}

pair <int,int> Tee::getLocation(){
    return location;
}

bool Tee::isDone(){
    return nextDirection == DONE;
}

pair <int,int> Tee::nextMove(){
    pair<int,int> next = location;
    int horz = 0, vert = 0;
    //The following switch statement is used to find
    // the next neighbors of the Tee's current location
    switch (nextDirection) {
        case UPRIGHT:
            horz += 2;
            vert += 2;
            nextDirection = RIGHT;
            break;
        case RIGHT:
            horz += 4;
            nextDirection = LOWRIGHT;
            break;
        case LOWRIGHT:
            horz += 2;
            vert -= 2;
            nextDirection = LOWLEFT;
            break;
        case LOWLEFT:
            horz -= 2;
            vert -= 2;
            nextDirection = LEFT;
            break;
        case LEFT:
            horz -= 4;
            nextDirection = UPLEFT;
            break;
        case UPLEFT:
            horz -= 2;
            vert += 2;
            nextDirection = DONE;
            break;
        default:
            break;
    }
    next.first +=vert;
    next.second += horz;
    return next;
}

/**************Board Functions***************/
Board::Board(){
    numPegs = 14;
    myBoard = vector<vector<int> >(5, vector<int>(9, INVALID));
    
    //creates default board
    myBoard[0][4] = HOLE;
    myBoard[1][3] = myBoard[1][5] = PEG;
    myBoard[2][2] = myBoard[2][4] = myBoard[2][6] = PEG;
    myBoard[3][1] = myBoard[3][3] = myBoard[3][5] = myBoard[3][7] = PEG;
    myBoard[4][0] = myBoard[4][2] = myBoard[4][4] = PEG;
    myBoard[4][6] = myBoard[4][8] = PEG;
}


vector<vector<int> > Board::getBoard(){
    return myBoard;
}


int Board::getNumPegs(){
    return numPegs;
}


bool Board::isPeg(pair<int,int> p){
    return myBoard[p.first][p.second] == PEG;
}

bool Board::isValidCoor(pair<int,int> p){
    bool toRe = false;
    //The following if blocks determine if the given location
    // is valid
    if (p.first >=0 && p.first < myBoard.size()) {
        if (p.second >=0 && p.second < myBoard[p.first].size()) {
            toRe = true;
        }
    }
    return toRe;
}

bool Board::isValidMove(pair <int,int> from, pair <int,int> to){
    bool toRe = false;
    //The following if blocks determine if the move to between
    // the given locatoins is valid.
    if (isValidCoor(from) && isValidCoor(to)) {
        if (myBoard[to.first][to.second] == HOLE) {
            int yCoor = (to.first - from.first)/2 + from.first;
            int xCoor = (to.second - from.second)/2 + from.second;
            if (isPeg(make_pair(yCoor,xCoor))) {
                toRe = true;
            }
        }
    }
    
    
    return toRe;
}

void Board::makeMove(pair <int,int> from, pair <int,int> to){
    //These lines "make the move" on the board by changing the
    //values of the given location to reflect the move
    myBoard[from.first][from.second] = HOLE;
    myBoard[to.first][to.second] = PEG;
    
    //These lines calculate the location in between the
    // given locations and adjust it on the board to
    // reflect the move
    int yCoor = (to.first - from.first)/2 + from.first;
    int xCoor = (to.second - from.second)/2 + from.second;
    myBoard[yCoor][xCoor] = HOLE;
    
    numPegs--;
}


ostream& operator<<(ostream& os, Board b){
    for (int y = 0; y < b.myBoard.size(); y++) {
        for (int x = 0; x < b.myBoard[y].size(); x++) {
            //The if/else block decides what character should
            // be printed
            if (b.myBoard[y][x] == b.PEG) {
                os << "X";
            }
            else if(b.myBoard[y][x] == b.HOLE) {
                os << "O";
            }
            else {
                os << " ";
            }
        }
        if (y < b.myBoard.size() - 1) {
            cout << endl;
        }
    }
    return os;
}


/*************Game Functions****************/

Game::Game(){
    solutionFound = false;
}

void Game::solve(Board b){
    //If only one peg is left, a solution has been found
    if (b.getNumPegs() == 1) {
        cout << b << endl;
        solutionFound = true;
    }
    else {
        //This forloop iterates through each possible location on the board
        for (int y = 0; y < b.getBoard().size() && !solutionFound; y++) {
            for (int x = 0; x < b.getBoard()[y].size() && !solutionFound; x++){
               
                //Each given location should only be checked for neighbors if
                //it is a PEG
                if (b.isPeg(make_pair(y,x))){
                    Tee t(y,x);
                    //This loop iterates through the possible neighbors of each
                    //Tee
                    while (!t.isDone() && !solutionFound) {
                        
                        pair <int,int> next = t.nextMove();
                        if (b.isValidMove(t.getLocation(),next)) {
                            
                            Board newBoard = b;
                            newBoard.makeMove(t.getLocation(), next);
                            solve(newBoard);
                            //The board is to be printed if a solution is found
                            if (solutionFound) {
                                cout << b << endl;
                            }
                        }
                    }
                }
            }
        }
    }
}
