#include <iostream>
#include <fstream>

using namespace std;

int main() {
    double addby = .1;
    
    ofstream fs("output.data");
    
    double i = 0;
    while(true) {
        fs << i;
        i += addby;
    }

    fs.close();
    return 0;
}
