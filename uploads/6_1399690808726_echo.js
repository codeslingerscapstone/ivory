var fs = require('fs');

var input = fs.readFileSync('/dev/stdin').toString().split('\n');

process.stdout.write(input.splice(1,input.length-1).join('\n'));
