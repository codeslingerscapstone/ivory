// Author: Kevin Smith
// Assignment: Program 1 (part 2)
// Description: Contains a single function that when multiplied over
// 		an inclusive range of 0 to N, produces the factorial of N.
// Compilation: gcc prog1.c f.c -o prog1

// Return 1 if x is zero in this case, in order to prevent the final result
// from always being 0.  Otherwise, simply return x as a double.
double f(int x) {
	if (x == 0) return (double) 1;
	return (double) x;
}